#!/usr/bin/env node
'use strict'

require('./src/main')(
    process.argv[2], // action
    process.argv[3], // type
    process.argv[4],  // name
    require(`${__dirname}/package.json`).version
);