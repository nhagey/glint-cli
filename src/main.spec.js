const main = require('./main.js')
const init = require('./init.js')
const component = require('./component.js')

describe('glint cli', () => {
    describe('main', () => {
        beforeEach(() => {
            spyOn(component, 'generate')
            spyOn(init, 'gitignore')
            spyOn(init, 'packageDependencies')
            spyOn(init, 'packageScripts')
            spyOn(init, 'packageName')
            spyOn(init, 'karmaConf')
            spyOn(init, 'polyfills')
            spyOn(init, 'readme')
            spyOn(init, 'githook')
        })
        it('gi go', () => {
            main('go')
            expect(init.gitignore).toHaveBeenCalled()
            expect(init.packageDependencies).toHaveBeenCalled()
            expect(init.packageScripts).toHaveBeenCalled()
            expect(init.packageName).toHaveBeenCalled()
            expect(init.karmaConf).toHaveBeenCalled()
            expect(init.polyfills).toHaveBeenCalled()
        })
        it('gi generate component', () => {
            main('generate', 'component', 'name', 'version')
            expect(component.generate).toHaveBeenCalledWith('name')
            expect(init.readme).toHaveBeenCalledWith('name', 'version')
        })
        it('gi generate component', () => {
            main('generate', 'githook', 'whatever', 'version')
            expect(init.githook).toHaveBeenCalledWith('whatever')
        })
    })
})