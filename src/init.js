'use strict'

const cp = require('child_process')
const fs = require('fs')
const file = require('./file')
const pascalCase = require('pascal-case')
const gh = require('@glint-ui/githook')

function githook (hook) {
    cp.execSync(`node node_modules/.bin/githook ${hook}`)
}

function karmaConf () {
    // karma -phantomjs launcher for bamboo
    file.update('karma.conf.js', {
        text: "\n      require('karma-phantomjs-launcher'),",
        after: "require('karma-chrome-launcher'),"
    })
}

function packageName () {
    cp.execSync('npm run scope:add')
}
function packagePublishConfig () {

    file.update('package.json', {
        removeProperty: 'private'
    })
    file.update('package.json', {
        data: {
            registry: 'https://glint.jfrog.io/glint/api/npm/glint-ui/'
        },
        updateProperty: 'publishConfig'
    })
}

function packageDependencies () {
    // Install package dependencies
    cp.execSync('npm install --save-dev karma-phantomjs-launcher')
    cp.execSync('npm install --save-dev npm-run-all')
    cp.execSync('npm install --save-dev http-server')
    cp.execSync('npm install --save-dev ng-packagr')
    cp.execSync('npm install --save-dev @compodoc/compodoc')
    cp.execSync('npm install --save-dev @glint-ui/apr-version')
    cp.execSync('npm install --save-dev @glint-ui/package-scope')
    cp.execSync('npm install --save-dev @glint-ui/shields')
    cp.execSync('npm install --save-dev @glint-ui/githook')
}

function packageScripts () {
    file.update('package.json', {
        data: {
            "build": "run-s build:package build:styles",
            "build:coverage": "karma start ./karma.conf.js --browsers PhantomJS  --single-run",
            "prebuild:coverage": "npm run test",
            "build:docs": "compodoc -d docs -p ./src/tsconfig.app.json -n $npm_package_name@$npm_package_version --disableCoverage",
            "build:package": "ng-packagr -p ng-package.json",
            "build:shields": "shields coverage/lcov.info",
            "build:styles": "# build:styles placeholder",
            "coverage": "http-server ./coverage -p 4100",
            "precoverage": "ng test --code-coverage --single-run",
            "docs": "http-server ./docs -p 4300",
            "predocs": "npm run build:docs",
            "ng": "ng",
            "start": "ng serve",
            "start:all": "run-p start docs coverage",
            "scope:add": "package-scope add @glint-ui",
            "scope:remove": "package-scope remove @glint-ui",
            "test": "run-p test:coverage test:e2e test:lint",
            "test:coverage": "ng test --code-coverage --single-run",
            "test:e2e": "ng e2e",
            "test:lint": "ng lint",
            "version:bump": "apr-version bump master",
            "version:push": "apr-version push master",
            "version:publish": "[[ -z $(npm info $npm_package_name@$npm_package_version) ]] && npm publish dist || echo $npm_package_name@$npm_package_version already exists"
        },
        updateProperty: 'scripts'
    })
}

function gitignore () {
    file.update('.gitignore', {
        text: '\n/docs',
        after: '# compiled output'
    })
}

function readme (name, version) {
    const template = fs.readFileSync(`${__dirname}/readme.template.md`, 'utf8')
    const md = template
        .replace(/{{name}}/mg, name)
        .replace(/{{PascalName}}/mg, pascalCase(name))
        .replace(/{{version}}/mg, version)
    file.update('README.md', {
        text: md,
        replace: /#\s.*/
    })
}

function polyfills () {
    file.update('src/polyfills.ts', {
        text: `/** IE9, IE10 and IE11 requires all of the following polyfills. **/
import 'core-js/es6/symbol';
import 'core-js/es6/object';
import 'core-js/es6/function';
import 'core-js/es6/parse-int';
import 'core-js/es6/parse-float';
import 'core-js/es6/number';
import 'core-js/es6/math';
import 'core-js/es6/string';
import 'core-js/es6/date';
import 'core-js/es6/array';
import 'core-js/es6/regexp';
import 'core-js/es6/map';
import 'core-js/es6/weak-map';
import 'core-js/es6/set';`,
        replace: /\/\*\* IE9.*([\s\S]*?)set';/ // https://regex101.com/r/VbMPqI/2
    })
}

module.exports = {
    githook,
    gitignore,
    karmaConf,
    packageName,
    packageScripts,
    packageDependencies,
    packagePublishConfig,
    polyfills,
    readme
}