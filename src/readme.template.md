![Alt text](./coverage.png)

# Glint UI {{name}} component

This component was generated with [Glint CLI](https://bitbucket.org/nhagey/glint-cli) version {{version}}.

## Installation

```
npm install --save-peer @glint-ui/{{name}}-component
```

Module integration.
```
import { {{PascalName}}Module } from ' @glint-ui/{{name}}';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    {{PascalName}}Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

## Usage

```
<glint-{{name}}></glint-{{name}}>
```

## Development

```
git clone git@bitbucket.org:awareteam/ui-{{name}}-component.git
```

Run `npm run docs` for a documentation server. Navigate to [localhost:4300](http://localhost:4300).

## NG Support