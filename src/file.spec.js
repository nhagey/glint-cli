const file = require('./file.js')
const fs = require('fs')

describe('file', () => {
    describe('update', () => {
        beforeEach(() => {
            spyOn(fs, 'readFileSync')
            spyOn(fs, 'writeFileSync')
        })
        it('throws an error when file is undefined', () => {
            expect(file.update).toThrow('file is required')
        })
        it('opens file', () => {
            file.update('myfile')
            expect(fs.readFileSync).toHaveBeenCalledWith('myfile', 'utf8')
        })
        it('doesn\'t require options', () => {
            file.update('myfile', {})
            expect(fs.writeFileSync).not.toHaveBeenCalled()
        })
    })
    describe('after option', () => {
        beforeEach(() => {
            spyOn(fs, 'readFileSync').andCallFake(() => {
                return '# compiled output'
            });
            spyOn(fs, 'writeFileSync')
        })
        it('saves with changes', () => {
            file.update('file', {
                text: '\nHi!',
                after: '# compiled output'
            })
            expect(fs.writeFileSync).toHaveBeenCalledWith('file', '# compiled output\nHi!', 'utf8')
        })
    })
    describe('data option', () => {
        beforeEach(() => {
            spyOn(fs, 'readFileSync').andCallFake(() => {
                return '{ "hello": {} }'
            });
            spyOn(fs, 'writeFileSync')
        })
        it('saves with updateProperty', () => {
            file.update('file', {
                data: { "hey": "there" },
                updateProperty: 'hello'
            })
            expect(fs.writeFileSync).toHaveBeenCalledWith('file', `{
  "hello": {
    "hey": "there"
  }
}`, 'utf8')
        })
        it('deletes property with removeProperty', () => {
            file.update('file', {
                removeProperty: 'hello'
            })
            expect(fs.writeFileSync).toHaveBeenCalledWith('file', `{}`, 'utf8')
        })
    })
    describe('replace option', () => {
        beforeEach(() => {
            // From ng 1.5.0 ng new project
            spyOn(fs, 'readFileSync').andCallFake(() => {
                return `
/** IE9, IE10 and IE11 requires all of the following polyfills. **/
// import 'core-js/es6/symbol';
// import 'core-js/es6/object';
// import 'core-js/es6/set';
`
            });
            spyOn(fs, 'writeFileSync')
        })
        it('saves with changes', () => {
            file.update('file', {
                text: 'Hi!',
                replace: /\/\*\* IE9.*([\s\S]*?)set';/
            })
            expect(fs.writeFileSync).toHaveBeenCalledWith('file', '\nHi!\n', 'utf8')
        })
    })
})