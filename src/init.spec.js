const init = require('./init.js')
const file = require('./file.js')
const fs = require('fs')
const cp = require('child_process')

describe('glint', () => {
    describe('init', () => {
        beforeEach(() => {
            spyOn(cp, 'execSync')
            spyOn(file, 'update')
            spyOn(fs, 'readFileSync').andCallFake(() => {
                return '# {{name}}'
            })
        })
        it('installs package dependencies', () => {
            init.packageDependencies()
            expect(cp.execSync).toHaveBeenCalledWith('npm install --save-dev karma-phantomjs-launcher')
            expect(cp.execSync).toHaveBeenCalledWith('npm install --save-dev npm-run-all')
            expect(cp.execSync).toHaveBeenCalledWith('npm install --save-dev http-server')
            expect(cp.execSync).toHaveBeenCalledWith('npm install --save-dev ng-packagr')
            expect(cp.execSync).toHaveBeenCalledWith('npm install --save-dev @compodoc/compodoc')
            expect(cp.execSync).toHaveBeenCalledWith('npm install --save-dev @glint-ui/apr-version')
            expect(cp.execSync).toHaveBeenCalledWith('npm install --save-dev @glint-ui/package-scope')
            expect(cp.execSync).toHaveBeenCalledWith('npm install --save-dev @glint-ui/shields')
            expect(cp.execSync).toHaveBeenCalledWith('npm install --save-dev @glint-ui/githook')
        })
        it('adds githook', () => {
            init.githook('whatever')
            expect(cp.execSync).toHaveBeenCalledWith('node node_modules/.bin/githook whatever')
        })
        it('adds glint scope to the package name', () => {
            init.packageName()
            expect(cp.execSync).toHaveBeenCalledWith('npm run scope:add')
        })
        it('adds glint publish config', () => {
            init.packagePublishConfig()
            expect(file.update).toHaveBeenCalledWith('package.json', {
                removeProperty: "private"
            })
            expect(file.update).toHaveBeenCalledWith('package.json', {
                data: {
                    registry: 'https://glint.jfrog.io/glint/api/npm/glint-ui/'
                },
                updateProperty: 'publishConfig'
            })
        })
        it('adds karma-phantomjs-reporter to karma.conf', () => {
            init.karmaConf()
            expect(file.update).toHaveBeenCalledWith('karma.conf.js', {
                text: "\n      require('karma-phantomjs-launcher'),",
                after: "require('karma-chrome-launcher'),"
            })
        })
        it('prepends readme template to readme', () => {
            init.readme('name')
            expect(file.update).toHaveBeenCalledWith('README.md', {
                text: '# name',
                replace: /#\s.*/
            })
        })
        it('adds scripts to package.json', () => {
            init.packageScripts()
            expect(file.update).toHaveBeenCalledWith('package.json', {
                data: { 
                    "build": "run-s build:package build:styles",
                    "build:coverage": "karma start ./karma.conf.js --browsers PhantomJS  --single-run",
                    "prebuild:coverage": "npm run test",
                    "build:docs": "compodoc -d docs -p ./src/tsconfig.app.json -n $npm_package_name@$npm_package_version --disableCoverage",
                    "build:package": "ng-packagr -p ng-package.json",
                    "build:shields": "shields coverage/lcov.info",
                    "build:styles": "# build:styles placeholder",
                    "coverage": "http-server ./coverage -p 4100",
                    "precoverage": "ng test --code-coverage --single-run",
                    "docs": "http-server ./docs -p 4300",
                    "predocs": "npm run build:docs",
                    "ng": "ng",
                    "start": "ng serve",
                    "start:all": "run-p start docs coverage",
                    "scope:add": "package-scope add @glint-ui",
                    "scope:remove": "package-scope remove @glint-ui",
                    "test": "run-p test:coverage test:e2e test:lint",
                    "test:coverage": "ng test --code-coverage --single-run",
                    "test:e2e": "ng e2e",
                    "test:lint": "ng lint",
                    "version:bump": "apr-version bump master",
                    "version:push": "apr-version push master",
                    "version:publish": "[[ -z $(npm info $npm_package_name@$npm_package_version) ]] && npm publish dist || echo $npm_package_name@$npm_package_version already exists"
                },
                updateProperty: 'scripts'
            })
        })
        it('adds /docs to file', () => {
            init.gitignore()
            expect(file.update).toHaveBeenCalledWith('.gitignore', {
                text: '\n/docs',
                after: '# compiled output'
            })
        })
        it('uncomments polyfills for PhantomJS', () => {
            init.polyfills()
            expect(file.update).toHaveBeenCalledWith('src/polyfills.ts', 
            {
                text: `/** IE9, IE10 and IE11 requires all of the following polyfills. **/
import 'core-js/es6/symbol';
import 'core-js/es6/object';
import 'core-js/es6/function';
import 'core-js/es6/parse-int';
import 'core-js/es6/parse-float';
import 'core-js/es6/number';
import 'core-js/es6/math';
import 'core-js/es6/string';
import 'core-js/es6/date';
import 'core-js/es6/array';
import 'core-js/es6/regexp';
import 'core-js/es6/map';
import 'core-js/es6/weak-map';
import 'core-js/es6/set';`,
                replace: /\/\*\* IE9.*([\s\S]*?)set';/
            })
        })
    })
})