const component = require('./component.js')
const cp = require('child_process')

describe('glint component', () => {
    describe('generate', () => {
        beforeEach(() => {
            spyOn(cp, 'exec')
        })
        it('throws an error when not called with a name', () => {
            expect(component.generate).toThrow('component name is required')
        })
        it('generates an angular module & component for "name"', () => {
            component.generate('name')
            expect(cp.exec).toHaveBeenCalledWith('ng generate module modules/name')
            expect(cp.exec).toHaveBeenCalledWith('ng generate component modules/name')
            expect(cp.exec).toHaveBeenCalledWith(`echo '{
  "$schema": "./node_modules/ng-packagr/ng-package.schema.json",
  "lib": {
    "entryFile": "ng-package-api.ts"
  }
}' > ng-package.json`)
            expect(cp.exec).toHaveBeenCalledWith(`echo "export * from './src/app/modules/name/name.module';" > ng-package-api.ts`)
        })
    })
})