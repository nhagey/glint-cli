const cp = require('child_process')

function generate (name) {
    if (!name) {
        throw new Error('component name is required')
    }
    cp.exec(`ng generate module modules/${name}`)
    cp.exec(`ng generate component modules/${name}`)
    cp.exec(`echo '{
  "$schema": "./node_modules/ng-packagr/ng-package.schema.json",
  "lib": {
    "entryFile": "ng-package-api.ts"
  }
}' > ng-package.json`)
    cp.exec(`echo "export * from './src/app/modules/${name}/${name}.module';" > ng-package-api.ts`)
}

module.exports = {
    generate
}
