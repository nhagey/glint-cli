const fs = require('fs')

function update (file, options) {
    if (!file) {
        throw new Error('file is required')
    }

    const src = fs.readFileSync(file, 'utf8')
    let newSrc, cmd, jsonSrc

    if (options) {
        cmd = options.replace ? 'replace' :
                options.after ? 'after' :
                options.updateProperty ? 'updateProperty' :
                options.removeProperty ? 'removeProperty' :
                undefined
    }

    switch (cmd) {
        case 'replace':
            newSrc = src.replace(options.replace, options.text)
            break
        case 'after':
            newSrc = src.replace(options.after, `${options.after}${options.text}`)
            break
        case 'updateProperty':
            jsonSrc = JSON.parse(src)
            jsonSrc[options.updateProperty] = options.data
            newSrc = JSON.stringify(jsonSrc, null, 2)
            break
        case 'removeProperty':
            jsonSrc = JSON.parse(src)
            delete jsonSrc[options.removeProperty]
            newSrc = JSON.stringify(jsonSrc, null, 2)
            break
    }

    if (cmd) {
        fs.writeFileSync(file, newSrc, 'utf8')
    }
}

module.exports = {
    update
}