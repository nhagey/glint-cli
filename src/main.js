const init = require('./init')
const component = require('./component')

module.exports = function (action, type, name, version) {
    switch (action) {
        case 'go':
            init.packageDependencies()
            init.packageScripts()
            init.packageName()
            init.packagePublishConfig()
            init.gitignore()
            init.karmaConf()
            init.polyfills()
            break
        case 'generate':
            switch (type) {
                case 'component':
                    component.generate(name)
                    init.readme(name, version)
                    break
                case 'githook':
                    init.githook(name)
            }
            break
        default:
            console.log('Passing me some parameters.')
            break
    }
}
