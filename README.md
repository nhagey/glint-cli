![Alt text](./coverage.png)

# Glint CLI

An exstension of [Angular CLI v1.5.0](https://cli.angular.io/) for Glint magic.

## Installation

```
npm install -g @glint-ui/gi
```

## Usage

Bootstrap NPM scripts with Glint CI scripts.

```
gi go
```

Bootstrap an Angular Component to import into Glint Apps.

```
gi generate component <component-name>
```

Add a [githook](https://bitbucket.org/nhagey/githook) to catch issues before CI does.

```
gi generate githook <git-hook>
```

## Example

Create Glint CI scripts to an Angular Project, and add a git hook.

Example
```
ng new widget
cd widget
gi go
gi generate component widget
gi generate githook git-push
```

## CI Scripts

After running `go` and `generate` the following npm scripts are available. You can also use ng scripts if you prefer.


Build code coverage shield for your readme.

```
npm run test:coverage
npm run build:shields
```

Serve your component.
```
npm start
```

Serve component docs.
```
npm run docs
```

Serve component coverage
```
npm run coverage
```

Serve all of the above
```
npm run start:all
```

Builds your component into an angular package so that others can import it.

```
npm run build
```

Publish your package to our private NPM registry.
```
npm publish
```